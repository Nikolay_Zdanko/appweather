//
//  NetworkServiceForSearchCityes.swift
//  weather
//
//  Created by Nikolay on 17.03.21.
//

import Foundation

class NetworkServiceForSearchCityes {

    func getRequest(url: String, comletion: @escaping ([ModelCityes]) -> Void) {
        sendRequest(urlString: url) { (data) in
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
               let json = try decoder.decode([ModelCityes].self, from: data)
                DispatchQueue.main.async {
                    comletion(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }

    func sendRequest( urlString: String, completion: @escaping (Data) -> Void) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if error == nil, let data = data {
            completion(data)
            } else {
                // нету инета ошибка
                print(error?.localizedDescription)
            }
        }.resume()
    }

}
