//
//  SupportViewController.swift
//  weather
//
//  Created by Nikolay on 5.04.21.
//

import UIKit
import JNGradientLabel

class SupportViewController: UIViewController {
// MARK: - IBOutlet
    @IBOutlet weak var myLabel: JNGradientLabel!
    
    let gradient = CAGradientLayer()

// MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        delayPushController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupGradientForLabel()
    }
}

private extension SupportViewController {
// MARK: - Setup gradient for label
    func setupGradientForLabel() {
        let leftColor = UIColor(red: (96 / 255.0), green: (255 / 255.0), blue: (207 / 255.0), alpha: 1)
        let rightColor = UIColor(red: (132 / 255.0), green: (89 / 255.0), blue: (255 / 255.0), alpha: 1)
        myLabel.setAxialGradientParameters(startPoint: CGPoint(x: 0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5), colors: [leftColor, rightColor], locations: [0, 1], options: .drawsBeforeStartLocation)
    }
    
// MARK: - Delay for present VC
    func delayPushController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func animationView() {
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        gradientAnimation.fromValue = [-0.5, -0.24, 0]
        gradientAnimation.toValue = [1.0, 1.25, 1.5]
        gradientAnimation.duration = 5.0
        gradientAnimation.autoreverses = true
        gradientAnimation.repeatCount = Float.infinity
        self.gradient.add(gradientAnimation, forKey: nil)
    }
}
