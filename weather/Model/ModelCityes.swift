//
//  ModelCityes.swift
//  weather
//
//  Created by Nikolay on 17.03.21.
//

import Foundation

struct ModelCityes: Decodable {
    var name: String
    var country: String
    var lat: Double
    var lon: Double
}
